OLD_VERSIONS := 5.1 5.2
CURRENT_VERSION := 5.3

CURRENT_TARGET := lua-cjson
OLD_TARGETS := $(addsuffix -cjson,$(addprefix lua,$(OLD_VERSIONS)))
TARGETS := $(join $(OLD_TARGETS),$(CURRENT_TARGET))

.PHONY: all $(CURRENT_TARGET) $(OLD_TARGETS)

all: $(CURRENT_TARGET) $(OLD_TARGETS)

$(CURRENT_TARGET):
	mkdir -p $@
	cp PKGBUILD.current $@/PKGBUILD
	sed -i 's/@LUA_VERSION@/$(CURRENT_VERSION)/g' $@/PKGBUILD
	cd $@ && makepkg -f && mksrcinfo

$(OLD_TARGETS):
	mkdir -p $(subst .,,$@)
	cp PKGBUILD $(subst .,,$@)
	sed -i 's/@LUA_VERSION@/$(firstword $(subst -, ,$(subst lua,,$@)))/g' $(subst .,,$@)/PKGBUILD
	cd $(subst .,,$@) && makepkg -f && mksrcinfo

