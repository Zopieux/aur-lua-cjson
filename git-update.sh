#!/bin/sh

for d in lua*cjson; do
	pushd $d >/dev/null
	git add .SRCINFO PKGBUILD
	git commit -m "$1"
	git push
	popd >/dev/null
done
